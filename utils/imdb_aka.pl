#!/usr/bin/perl -w

# AKA queries are in the form:  http://www.imdb.com/find?q=divinas%20tentaciones;s=tt;site=aka
#
# TODO: Fetch and store original airdate (year)
#
# If the result is successful, the client is automatically redirected to the page for the movie, so the
# data can be parsed right from there. An example in sample_data/imdb-aka-OK.html
# Otherwise, a set of possible results is shown (sample_data/imdb-aka-notexact.html)

use strict;

use WWW::Mechanize;
use DBI;

my $qurl = q|http://www.imdb.com/find?q=QUERY_HERE;s=tt;site=aka|;

# Initialize web bot
my $bot = new WWW::Mechanize();
$bot->agent_alias('Linux Mozilla');

# Read the database connection information from ~/.mythtv/mysql.txt and parse it
open MYTHCONF, "< $ENV{HOME}/.mythtv/mysql.txt" or die "Couldn't find mythtv mysql config file";
my @text = <MYTHCONF>;
chomp(@text);
my $confString = join ",", @text;
$confString =~ /DBUserName=([^,]*)/;
my $DBUserName = $1;
$confString =~ /DBName=([^,]*)/;
my $DBName = $1;
$confString =~ /DBPassword=([^,]*)/;
my $DBPassword = $1;
$confString =~ /DBHostName=([^,]*)/;
my $DBHostName = $1;

# Connect to the database
my $dbh = DBI->connect("dbi:mysql:".$DBName.":".$DBHostName.":3306", $DBUserName, $DBPassword) || die "Database connection failed";

# Get all the titles, starting from the ones showing now
my $stmt = "select distinct title from program where category_type = 'movie' and description = '' and starttime > now() order by starttime";
my $sth = $dbh->prepare($stmt) or die "Couldn't prepare statement: " . $dbh->errstr;
$sth->execute();
my $data = $sth->fetchall_arrayref([0]);

print "There are " . @$data . " movie titles\n";
my $i = 0;
while(my $title = shift(@$data)) {
	$i++;
	$title = $title->[0];
	
	# Search movie data in IMDb
	my $movieData = get_movie($title);
	
	# Store results in the database
	$stmt = "update program set description=". $dbh->quote($movieData->{'Plot'}) .", stars=". $movieData->{'Stars'} ." where title='". $title . "'";
	$dbh->do($stmt);
	
	# Show some progress
	if($i % 10 == 0) {
		print $i . " movie titles resolved\n";
	}
}

$dbh->disconnect;

# Input
#   scalar: movie name
# Output if successful
#   hash: {
#		Plot => 'the plot',
#       Stars => 0 - 1 (mapped from 0 - 10)
#   }
sub get_movie {
	my ($aka_name) = @_;
	
	my $query = $qurl;
	$query =~ s/QUERY_HERE/$aka_name/;
	$bot->get($query);
	
	if( $bot->content() =~ /link rel="canonical" href="http:\/\/www.imdb.com\/title\//m ) {
		return get_details($bot->content());
	} else {
		# TODO: If there is a clear winner in the list, go fetch and parse that one
		#if($bot->content() =~ /title\/([^\/]*)\/.{0,8}>[^<]*<\/a>[^<]*<br>&#160;aka <em>\"$aka_name\"/mi) {
		#	print "Failed but found = ".$1."\n";
		#}
		
		# Return default response if single result was not found
		return { "Plot" => "Description not found", "Stars" => 0 }
	}
}

# Given the contents of a known movie page, parse the data out
sub get_details {
	my ($content) = @_;
	
	# Retrieve plot (and actors and directors) from HTML parameters
	$content =~ /name="description" content="([^"]*)Visit IMDb/m;
	my $plot = $1;
	# Remove directors and place actors at the end
	if( $plot =~ /Directed by [^.]*\.\s*([^.]*\.)\s*(.*)/) {
		$plot = $2.$1;
	}
	
	# Parse stars and convert from 0-10 to 0-1
	$content =~ />([0-9.]{1,4})\/10</m;
	my $stars = $1 / 10.0;
	
	return {"Plot" => $plot, "Stars" => $stars };
}

