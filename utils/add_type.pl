#!/usr/bin/perl

# This program runs over the already filled mythtv database and tries to mark movies as such
# This is done by looking for programs that last more than 90 minutes and fall in one of a
# set of categories and then, adding the value 'movie' in the column 'category_type' 

use DBI;

# Start by reading the database connection information from ~/.mythtv/mysql.txt and parsing it
open MYTHCONF, "< $ENV{HOME}/.mythtv/mysql.txt" or die "Couldn't find mythtv mysql config file";
my @text = <MYTHCONF>;
chomp(@text);
my $confString = join ",", @text;
$confString =~ /DBUserName=([^,]*)/;
my $DBUserName = $1;
$confString =~ /DBName=([^,]*)/;
my $DBName = $1;
$confString =~ /DBPassword=([^,]*)/;
my $DBPassword = $1;
$confString =~ /DBHostName=([^,]*)/;
my $DBHostName = $1;

# Connect to the database
my $dbh = DBI->connect("dbi:mysql:".$DBName.":".$DBHostName.":3306", $DBUserName, $DBPassword) || die "Database connection failed";

# And assert the one statement that flags moves as such
my $stmt = "update program " .
"set category_type = 'movie' " .
"where " .
"(UNIX_TIMESTAMP(endtime) - UNIX_TIMESTAMP(starttime))/60 > 100 " .
"and category in " .
"('Drama','Comedia','Thriller','Acción','Terror','C.Romantica','Policial','C.Ficción'," .
"'Fantasía','Animación','Infantil','Aventura','C.Dramática','Western','Biográfico'," .
"'D.Animados','Crimen','Romántica','Erótica','Suspenso','Familiar','Bélica') " .
"and category_type <> 'movie'";
$dbh->do($stmt);

$dbh->disconnect;



