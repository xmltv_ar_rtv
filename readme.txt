This project is being hosted in git repo at: http://repo.or.cz/w/xmltv_ar_rtv.git

=================================================================================

In order to run tv_grab_ar_rtv you need to have XMLTV installed and configured.
I don't remember how I got about it, since I already had it running in my development
environment when I started documenting this.
What I did notice is that it automatically uses a configuration file out of my
~/.xmltv/ directory, particularly from the ~/.xmltv/tv_grab_ar_rtv.conf file.

= On-going work =

== Marcando películas como tales para búsquedas ==

- Las películas aparecen en el search si tienen 'movies' en el campo 'category_type' de la tabla 'program' (ésto lo vi en el archivo /etc/mythtv/mythweb-canned_searches.conf.php)
- Las categorías que vienen de reporTV van a parar al campo 'category'
- La única forma que parece posible para detectar una película es si el programa mide más de 2 horas

Query para encontrar películas que se puedan marcar:

--
select
	*, (UNIX_TIMESTAMP(endtime) - UNIX_TIMESTAMP(starttime))/60 as duration
from
	program
where
	(UNIX_TIMESTAMP(endtime) - UNIX_TIMESTAMP(starttime))/60 > 100
and
	category in (
		'Drama','Comedia','Thriller','Acción','Terror','C.Romantica','Policial','C.Ficción',
		'Fantasía','Animación','Infantil','Aventura','C.Dramática','Western','Biográfico',
		'D.Animados','Crimen','Romántica','Erótica','Suspenso','Familiar','Bélica')
--

Como la duración la calcula automáticamente mythtv, ésto lo tengo que hacer como un script aparte
que corra sobre la base de datos después de cargar los datos